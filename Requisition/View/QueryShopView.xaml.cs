﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using Requisition.Model;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Requisition.View
{
    public partial class QueryShopView : PhoneApplicationPage, INotifyPropertyChanged
    {
        public QueryShopView()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private ObservableCollection<RequestReport> _queryRequestDetails;
        public ObservableCollection<RequestReport> QueryRequestDetails
        {
            get
            {
                if (_queryRequestDetails == null)
                {
                    _queryRequestDetails = new ObservableCollection<RequestReport>();
                }

                return _queryRequestDetails;
            }

            set
            {
                if (_queryRequestDetails != value)
                {
                    _queryRequestDetails = value;
                    NotifyPropertyChanged("QueryRequestDetails");
                }
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                var requestList = from detail in db.details
                                  select new
                                  {
                                      detail.Request.IdRequest,
                                      detail.Request.Client.Name,
                                      detail.Request.Client.MiddleName,
                                      detail.Request.Client.LastName,
                                      detail.Item,
                                      detail.Amount,
                                      detail.Request.RequestDate
                                  };

                int size = requestList.Count();
                int i = 0;
                MyWebService.RequestDetail[] items = new MyWebService.RequestDetail[size];
                foreach (var request in requestList)
                {
                    items[i] = new MyWebService.RequestDetail();
                    items[i].IdRequest = request.IdRequest + "";
                    items[i].FullName = request.Name + " " +
                                        request.MiddleName + " " +
                                        request.LastName;
                    items[i].Item = request.Item;
                    items[i].Amount = request.Amount + "";
                    items[i].RequestDate = request.RequestDate.ToString();
                    i++;
                }

                var requestDetails = processReports(items);
                QueryRequestDetails = new ObservableCollection<RequestReport>(requestDetails);
            }

            base.OnNavigatedTo(e);
        }

        private ObservableCollection<RequestReport> processReports(MyWebService.RequestDetail[] result)
        {
            int prev = 0;
            ObservableCollection<RequestReport> reports = new ObservableCollection<RequestReport>();
            RequestReport report = new RequestReport();
            ObservableCollection<ItemDetail> details = new ObservableCollection<ItemDetail>();
            bool first = true;
            foreach (var row in result)
            {
                int id = Int32.Parse(row.IdRequest);
                if (id != prev)
                {
                    if (!first) {
                        report.Items = new ObservableCollection<ItemDetail>(details);
                        details.Clear();
                        reports.Add(report);
                        report = new RequestReport();
                    }

                    first = false;
                    prev = id;
                    report.IdRequest = id + "";
                    report.ClientName = row.FullName;
                    report.Date = row.RequestDate;
                    details.Add(new ItemDetail { Amount = row.Amount, Name = row.Item });
                }
                else
                {
                    details.Add(new ItemDetail { Amount = row.Amount, Name = row.Item });
                }
            }

            if (details.Count > 0)
            {
                report.Items = new ObservableCollection<ItemDetail>(details);
                reports.Add(report);
            }

            return reports;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class RequestReport : INotifyPropertyChanged
    {
        public RequestReport() { }

        private string _idRequest;
        public string IdRequest
        {
            get
            {
                return _idRequest;
            }

            set
            {
                if (_idRequest != value)
                {
                    _idRequest = value;
                    NotifyPropertyChanged("IdRequest");
                }
            }
        }

        private string _clientName;
        public String ClientName
        {
            get
            {
                return _clientName;
            }

            set
            {
                if (_clientName != value)
                {
                    _clientName = value;
                    NotifyPropertyChanged("ClientName");
                }
            }
        }

        private string _date;
        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                if (_date != value)
                {
                    _date = value;
                    NotifyPropertyChanged("Date");
                }
            }
        }

        private ObservableCollection<ItemDetail> _items;
        public ObservableCollection<ItemDetail> Items
        {
            get
            {
                return _items;
            }

            set
            {
                if (_items != value)
                {
                    _items = value;
                    NotifyPropertyChanged("Items");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class ItemDetail : INotifyPropertyChanged
    {
        public ItemDetail()
        {
        }

        private string _amount;
        public string Amount
        {
            get
            {
                return _amount;
            }

            set
            {
                if (_amount != value)
                {
                    _amount = value;
                    NotifyPropertyChanged("Amount");
                }
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                if (_name != value)
                {
                    _name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}