﻿using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Requisition.Model;
using System.Diagnostics;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Collections.ObjectModel;

namespace Requisition.View
{
    public partial class AddClientView : PhoneApplicationPage, INotifyPropertyChanged
    {
        public AddClientView()
        {
            InitializeComponent();
            this._client = new Client();
            this.DataContext = this;
        }


        private Client _client;
        public Client Client
        {
            get { return _client; }
            set
            {
                if (_client != value)
                {
                    _client = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(Client.Name) ||
                String.IsNullOrEmpty(Client.MiddleName) ||
                String.IsNullOrEmpty(Client.LastName) ||
                String.IsNullOrEmpty(Client.Address))
            {
                MessageBox.Show("ERROR: Te faltan dados.");
                return;
            }

            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                var keys = (from Key k in db.keys select k).First();
                Client newClient = new Client
                {
                    IdClient = keys.NewIdClient,
                    Name = Client.Name,
                    MiddleName = Client.MiddleName,
                    LastName = Client.LastName,
                    Address = Client.Address
                };

                keys.NewIdClient += 1;
                db.clients.InsertOnSubmit(newClient);

                bool ok = true;
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception) { ok = false; }

                if (ok)
                {
                    MessageBox.Show("El cliente ha sido agregado.");
                    Client.Name = String.Empty;
                    Client.MiddleName = String.Empty;
                    Client.LastName = String.Empty;
                    Client.Address = String.Empty;
                }
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                db.SubmitChanges();
            }
        }
    }
}