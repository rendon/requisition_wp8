﻿using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Requisition.Model;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Requisition.View
{
    public partial class ShopView : PhoneApplicationPage, INotifyPropertyChanged
    {
        public ShopView()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private ObservableCollection<Detail> _detailItems;
        public ObservableCollection<Detail> DetailItems
        {
            get
            {
                if (_detailItems == null)
                {
                    _detailItems = new ObservableCollection<Detail>();
                }

                return _detailItems;
            }

            set
            {
                if (_detailItems != value)
                {
                    _detailItems = value;
                    NotifyPropertyChanged("DetailItems");
                }
            }
        }

        private string _newItem;
        public string NewItem
        {
            get
            {
                if (_newItem == null)
                {
                    _newItem = String.Empty;
                }

                return _newItem;
            }

            set
            {
                if (_newItem != value)
                {
                    _newItem = value;
                    NotifyPropertyChanged("NewItem");
                }
            }
        }

        private string _newItemAmount;
        public string NewItemAmount
        {
            get
            {
                if (_newItemAmount == null)
                {
                    _newItemAmount = String.Empty;
                }
                return _newItemAmount;
            }

            set
            {
                if (_newItemAmount != value)
                {
                    _newItemAmount = value;
                    NotifyPropertyChanged("NewItemAmount");
                }
            }
        }

        private string _idClient;
        public string IdClient
        {
            get
            {
                if (_idClient == null)
                {
                    _idClient = String.Empty;
                }

                return _idClient;
            }

            set
            {
                if (_idClient != value)
                {
                    _idClient = value;
                    NotifyPropertyChanged("IdClient");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void addItemButton_Click(object sender, RoutedEventArgs e)
        {

            if (String.IsNullOrEmpty(NewItem))
            {
                return;
            }

            int amount = 0;
            try
            {
                amount = Int32.Parse(NewItemAmount);
            }
            catch (FormatException fe)
            {
                return;
            }

            DetailItems.Add(new Detail
            {
                Item = NewItem,
                Amount = amount
            });

            NewItem = String.Empty;
            NewItemAmount = String.Empty;
        }

        private void saveRequisitionButton_Click_1(object sender, RoutedEventArgs e)
        {
            if (DetailItems.Count == 0)
            {
                MessageBox.Show("La lista esta vacía.");
                return;
            }

            int id = 0;
            try
            {
                id = Int32.Parse(IdClient);
            }
            catch (FormatException fe)
            {
                MessageBox.Show("Verifica el ID del cliente.");
                return;
            }


            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                string date = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;
                var keys = (from Key k in db.keys select k).First();
                int newRequestId = keys.NewIdRequest;
                int year = DateTime.Now.Year;
                int month = DateTime.Now.Month;
                int day = DateTime.Now.Day;
                int hour = DateTime.Now.Hour;
                int minute = DateTime.Now.Minute;
                int second = DateTime.Now.Second;
                Request request = new Request
                {
                    IdRequest = newRequestId,
                    IdClient = id,
                    RequestDate = new DateTime(year, month, day, hour, minute, second)
                };

                keys.NewIdRequest += 1;
                db.requests.InsertOnSubmit(request);
                bool ok = true;
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ERROR: " + ex.ToString());
                    ok = false;
                }

                if (ok)
                {
                    var result = from r in db.requests
                                 select r;

                    foreach (Detail detail in DetailItems)
                    {
                        db.details.InsertOnSubmit(new Detail
                        {
                            IdDetail = keys.NewIdDetail,
                            IdRequest = newRequestId,
                            Item = detail.Item,
                            Amount = detail.Amount
                        });

                        keys.NewIdDetail += 1;
                    }

                    try
                    {
                        db.SubmitChanges();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("ERROR: " + ex.ToString());
                        ok = false;
                    }

                    if (!ok)
                    {
                        MessageBox.Show("¡ERROR: No se pudo guardar el pedido!");
                    }
                }
                else
                {
                    MessageBox.Show("¡ERROR: No se pudo guardar el pedido!");
                }

                if (ok)
                {
                    MessageBox.Show("La requisicón se ha guardado.");
                    IdClient = String.Empty;
                    DetailItems.Clear();
                }
            }
        }

    }
}