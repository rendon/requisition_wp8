﻿using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Requisition.Model;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Requisition.View
{
    public partial class QueryClientView : PhoneApplicationPage, INotifyPropertyChanged
    {
        public QueryClientView()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                var clients = from Client client in db.clients
                              select client;

                ObservableCollection<Client> items = new ObservableCollection<Client>();
                foreach (Client c in clients)
                {
                    items.Add(c);
                }

                ClientItems = new ObservableCollection<Client>(items);
            }

        }

        private ObservableCollection<Client> _clientItems;
        public ObservableCollection<Client> ClientItems
        {
            get
            {
                return _clientItems;
            }

            set
            {
                if (_clientItems != value)
                {
                    _clientItems = value;
                    NotifyPropertyChanged("ClientItems");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}