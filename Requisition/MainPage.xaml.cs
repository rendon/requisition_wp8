﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Requisition.Resources;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using Requisition.Model;
using System.Diagnostics;
using System.Threading;
using System.Collections.ObjectModel;

namespace Requisition
{
    public partial class MainPage : PhoneApplicationPage
    {
        private MyWebService.RequestPortTypeClient ws;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            Loaded += MainPage_Loaded;
            ws = new MyWebService.RequestPortTypeClient();
            ws.GetAllClientsCompleted += ws_GetAllClientsCompleted;
            ws.GetAllRequestsCompleted += ws_GetAllRequestsCompleted;
            ws.GetAllDetailsCompleted += ws_GetAllDetailsCompleted;
            ws.SynchronizeCompleted += ws_SynchronizeCompleted;

        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
        }

        private static void SetProgressIndicator(bool isVisible)
        {
            SystemTray.ProgressIndicator.IsIndeterminate = isVisible;
            SystemTray.ProgressIndicator.IsVisible = isVisible;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                base.OnNavigatedTo(e);
                if (db.DatabaseExists())
                {
                    if (db.keys.Count() == 0)
                    {
                        updateKeys();
                    }
                }
            }
        }

        private void addClientButton_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/AddClientView.xaml",
                UriKind.RelativeOrAbsolute));
        }

        private void queryClientButton_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/QueryClientView.xaml",
                UriKind.RelativeOrAbsolute));
        }

        private void shopButton_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/ShopView.xaml",
                UriKind.RelativeOrAbsolute));
        }


        private void queryShopButton_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/View/QueryShopView.xaml",
                UriKind.RelativeOrAbsolute));
        }

        private void syncOperation_Click_1(object sender, EventArgs e)
        {
            SetProgressIndicator(true);
            SystemTray.ProgressIndicator.Text = "Sincronizando datos con el servidor.";
            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                var C = from c in db.clients select c;
                var R = from r in db.requests select r;
                var D = from d in db.details select d;

                MyWebService.Client[] clients = new MyWebService.Client[C.Count()];
                MyWebService.Request[] requests = new MyWebService.Request[R.Count()];
                MyWebService.Detail[] details = new MyWebService.Detail[D.Count()];

                int i = 0;
                foreach (var c in C)
                {
                    clients[i] = new MyWebService.Client();
                    clients[i].IdClient = c.IdClient + "";
                    clients[i].Name = c.Name;
                    clients[i].MiddleName = c.MiddleName;
                    clients[i].LastName = c.LastName;
                    clients[i].Address = c.Address;
                    i++;
                }

                i = 0;
                foreach (var r in R)
                {
                    requests[i] = new MyWebService.Request();
                    requests[i].IdRequest = r.IdRequest + "";
                    requests[i].IdClient = r.IdClient + "";
                    requests[i].RequestDate = MySqlDateFormat(r.RequestDate);
                    i++;
                }

                i = 0;
                foreach (var d in D)
                {
                    details[i] = new MyWebService.Detail();
                    details[i].IdDetail = d.IdDetail + "";
                    details[i].IdRequest = d.IdRequest + "";
                    details[i].Item = d.Item;
                    details[i].Amount = d.Amount + "";
                    i++;
                }

                ws.SynchronizeAsync(clients, requests, details);
            }
        }

        void ws_SynchronizeCompleted(object sender, MyWebService.SynchronizeCompletedEventArgs e)
        {
            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                if (db.DatabaseExists())
                {
                    db.DeleteDatabase();
                    db.CreateDatabase();
                }
            }

            ws.GetAllClientsAsync();
        }

        void ws_GetAllClientsCompleted(object sender, MyWebService.GetAllClientsCompletedEventArgs e)
        {
            Collection<Client> newClients = new Collection<Client>();
            foreach (var row in e.Result)
            {
                Client c = new Client
                {
                    IdClient = Int32.Parse(row.IdClient),
                    Name = row.Name,
                    MiddleName = row.MiddleName,
                    LastName = row.LastName,
                    Address = row.Address
                };

                newClients.Add(c);
            }


            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                db.clients.InsertAllOnSubmit(newClients);
                db.SubmitChanges();

                ws.GetAllRequestsAsync();
            }
        }


        void ws_GetAllRequestsCompleted(object sender, MyWebService.GetAllRequestsCompletedEventArgs e)
        {
            Collection<Request> newRequests = new Collection<Request>();
            foreach (var row in e.Result)
            {
                Request r = new Request
                {
                    IdRequest = Int32.Parse(row.IdRequest),
                    IdClient = Int32.Parse(row.IdClient),
                    RequestDate = SQLServerDateFormat(row.RequestDate)
                };
                newRequests.Add(r);
            }

            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                db.requests.InsertAllOnSubmit(newRequests);
                db.SubmitChanges();

                ws.GetAllDetailsAsync();
            }
        }

        private DateTime SQLServerDateFormat(string rd)
        {
            string[] tokens = rd.Split(new char[] { '-', ' ', ':' });
            int year = Int32.Parse(tokens[0]);
            int month = Int32.Parse(tokens[1]);
            int day = Int32.Parse(tokens[2]);
            int hour = Int32.Parse(tokens[3]);
            int minute = Int32.Parse(tokens[4]);
            int second = Int32.Parse(tokens[5]);

            return new DateTime(year, month, day, hour, minute, second);
        }

        void ws_GetAllDetailsCompleted(object sender, MyWebService.GetAllDetailsCompletedEventArgs e)
        {
            Collection<Detail> newDetails = new Collection<Detail>();
            foreach (var row in e.Result)
            {
                Detail d = new Detail
                {
                    IdDetail = Int32.Parse(row.IdDetail),
                    IdRequest = Int32.Parse(row.IdRequest),
                    Item = row.Item,
                    Amount = Int32.Parse(row.Amount)
                };
                newDetails.Add(d);
            }

            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                db.details.InsertAllOnSubmit(newDetails);
                db.SubmitChanges();

            }

            updateKeys();

            SetProgressIndicator(false); 
        }

        private void updateKeys()
        {
            int idClient = 1;
            int idRequest = 1;
            int idDetail = 1;
            string cs = RequisitionDataContext.DBConnectionString;
            using (RequisitionDataContext db = new RequisitionDataContext(cs))
            {
                if (db.clients.Count() > 0)
                {
                    var id = db.clients.Max(x => x.IdClient);
                    idClient = Math.Max(idClient, id + 1);
                }

                if (db.requests.Count() > 0)
                {
                    var id = db.requests.Max(x => x.IdRequest);
                    idRequest = Math.Max(idRequest, id + 1);
                }

                if (db.details.Count() > 0)
                {
                    var id = db.details.Max(x => x.IdDetail);
                    idDetail = Math.Max(idDetail, id + 1);
                }

                Key current = new Key();
                if (db.keys.Count() > 0)
                {
                    current = (from Key k in db.keys select k).First();
                }

                current.NewIdClient = idClient;
                current.NewIdRequest = idRequest;
                current.NewIdDetail = idDetail;

                db.keys.InsertOnSubmit(current);
                db.SubmitChanges();
            }
        }

        private string MySqlDateFormat(DateTime dateTime)
        {
            return dateTime.Year + "-"
                    + dateTime.Month + "-"
                    + dateTime.Day + " "
                    + dateTime.Hour + ":"
                    + dateTime.Minute + ":"
                    + dateTime.Second;
        }

    }

    public class RequisitionDataContext : DataContext
    {
        public static string DBConnectionString =
            "Data Source=isostore:/Requisitions.sdf";
        public RequisitionDataContext(string connectionString)
            : base(connectionString)
        { }

        public Table<Client> clients;
        public Table<Detail> details;
        public Table<Request> requests;
        public Table<Key> keys;
    }
}