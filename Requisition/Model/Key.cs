﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Requisition.Model
{
    [Table]
    public class Key
    {
        [Column(IsPrimaryKey = true)]
        public int TheKey; // A work around

        [Column]
        public int NewIdClient;

        [Column]
        public int NewIdRequest;

        [Column]
        public int NewIdDetail;
    }
}
