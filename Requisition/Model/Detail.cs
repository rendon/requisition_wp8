﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Requisition.Model
{
    [Table]
    public class Detail : INotifyPropertyChanged
    {
        private int _idDetail;
        [Column(IsPrimaryKey = true, IsDbGenerated = false,
            DbType = "INT NOT NULL", CanBeNull = false,
            AutoSync = AutoSync.OnInsert)]
        public int IdDetail
        {
            get { return _idDetail; }
            set { _idDetail = value; }
        }

        [Column]
        public int IdRequest;
        private EntityRef<Request> _request;
        [Association(
            Name = "FK_Detail_Request",
            IsForeignKey = true,
            Storage = "_request",
            ThisKey = "IdRequest")]
        public Request Request
        {
            get { return _request.Entity; }
            set { _request.Entity = value; }
        }

        private string _item;
        [Column]
        public string Item
        {
            get { return _item; }
            set { _item = value; }
        }


        private int _amount;
        [Column]
        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
