﻿using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Requisition.Model
{
    [Table]
    public class Client : INotifyPropertyChanged
    {
        public Client()
        {
            this._requests = new EntitySet<Request>();
        }

        private int _idClient;
        [Column(IsPrimaryKey = true, IsDbGenerated = false,
            DbType = "INT NOT NULL", CanBeNull = false,
            AutoSync = AutoSync.OnInsert)]
        public int IdClient
        {
            get { return _idClient; }
            set
            {
                if (_idClient != value)
                {
                    _idClient = value;
                    NotifyPropertyChanged("IdClient");
                }
            }
        }

        private EntitySet<Request> _requests;
        [Association(Storage = "_requests", OtherKey = "IdClient")]
        public EntitySet<Request> Requests
        {
            get { return _requests; }
            set { _requests.Assign(value); }
        }

        private string _name;
        [Column]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        private string _middleName;
        [Column]
        public string MiddleName
        {
            get { return _middleName; } 
            set
            {
                if (_middleName != value)
                {
                    _middleName = value;
                    NotifyPropertyChanged("MiddleName");
                }
            }
        }


        private string _lastName;
        [Column]
        public string LastName
        {
            get { return _lastName; } 
            set
            {
                if (_lastName != value)
                {
                    _lastName = value;
                    NotifyPropertyChanged("LastName");
                }
            }
        }

        private string _address;
        [Column]
        public string Address
        {
            get { return _address; } 
            set
            {
                if (_address != value)
                {
                    _address = value;
                    NotifyPropertyChanged("Address");
                }
            }
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

    }

}
