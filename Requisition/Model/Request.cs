﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.ComponentModel;

namespace Requisition.Model
{
    [Table]
    public class Request : INotifyPropertyChanged
    {
        public Request()
        {
            this._details = new EntitySet<Detail>();
        }

        private int _idRequest;
        [Column(IsPrimaryKey = true,
            IsDbGenerated = false,
            DbType = "INT NOT NULL",
            CanBeNull = false)]
        public int IdRequest
        {
            get { return _idRequest; }
            set { _idRequest = value; }
        }

        EntitySet<Detail> _details;
        [Association(Storage = "_details", OtherKey = "IdRequest")]
        public EntitySet<Detail> Details
        {
            get { return _details; }
            set { _details.Assign(value); }
        }

        [Column]
        public int IdClient;

        private EntityRef<Client> _client;
        [Association(
            Name = "FK_Requests_IdClient",
            IsForeignKey = true,
            Storage = "_client",
            ThisKey="IdClient")]
        public Client Client
        {
            get { return _client.Entity; }
            set { _client.Entity = value; }
        }

        private DateTime _date;
        [Column]
        public DateTime RequestDate
        {
            get { return _date; } 
            set { _date = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
