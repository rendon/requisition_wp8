Requisition Windows Phone 8
===========================
A simple Windows Phone App to control buy's requests. This application is coupled with my other project [Request_WS](https://bitbucket.org/rendon/request_ws).

For more information see the project write up (in Spanish): [http://rendon.x10.mx/?p=875](http://rendon.x10.mx/?p=875).

License
=======
The license of this project is GPLv3. With a possible exception for code generated by Visual Studio to comunicate with the Web service, I don't know which policies apply.
